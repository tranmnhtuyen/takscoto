import { ResponeResult } from './../Models/ResponeResult';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ROOT_URL, Password } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenService {
  public baseURL = ROOT_URL;
  constructor(
    public http:HttpClient,
    public router:Router,
  ) { }

  

  //hàm kiểm tra login
  checkLogin(userName:string,password:string){
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = {userName: userName, password: password};
    console.log(JSON.stringify(body))
    return this.http.post<ResponeResult>(this.baseURL+"api/login",JSON.stringify(body),{headers,withCredentials:true})


  }

  logout() {
    try {
      const body = {};
      this.http.post<ResponeResult>(this.baseURL + 'api/logout', body).subscribe(res => {
        if (res && res.isOk) {
          this.clearStorage();
          return true;
        }
      });
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  clearStorage() {
    localStorage.clear();
    this.router.navigate(['/login']).then(r => {

    });
  }
}
