import { HttpClient } from '@angular/common/http';
import { ROOT_URL } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public baseURL = ROOT_URL;
  constructor(
    public http: HttpClient
  ) { }

  //get dữ liệu user từ id
  getUser(id: Number) {
    console.log(this.baseURL + "api/users/" + id)
    return this.http.get(this.baseURL + "api/users/" + id);
  }

  //get all dữ liệu user
  getListUser() {
    return this.http.get(this.baseURL + "api/users");
  }
}
