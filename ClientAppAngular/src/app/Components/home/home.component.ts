import { AuthenService } from './../../Services/authen.service';
import { UserService } from './../../Services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    public _userService:UserService,
    public router:Router,
    public authen:AuthenService
  ) { }

  ngOnInit(): void {
    this.loadData()
  }

  user:any;
  loadData(){
    let id:Number = + localStorage.getItem('userId');
    this._userService.getUser(id).subscribe(data => {
      this.user = data;
    })
  }

  Logout(){
    this.authen.logout();
  }

}
