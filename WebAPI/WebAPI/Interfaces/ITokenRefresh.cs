﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Interfaces
{
    public interface ITokenRefresh
    {
        TokenRefresh GetData(string Key);

        bool Save(TokenRefresh tokenRefresh, bool isNew);
    }
}
