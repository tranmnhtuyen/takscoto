﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Cryptography;
//using System.Text;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Configuration;
//using WebAPI.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Microsoft.VisualBasic.CompilerServices;
using WebAPI.Utils;
using System.Security.Cryptography;
using WebAPI.Interfaces;
namespace WebAPI.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    //public class LoginController : ControllerBase
    //{
    //    private readonly SIMMSContext _context;

    //    public LoginController(SIMMSContext context)
    //    {
    //        _context = context;
    //    }

    //    [HttpPost]
    //    public bool PostUser(User user)
    //    {

    //        StringBuilder hash = new StringBuilder();
    //        MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
    //        byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(user.Password));

    //        for (int i = 0; i < bytes.Length; i++)
    //        {
    //            hash.Append(bytes[i].ToString("x2"));
    //        }


    //        return _context.User.Any(e => e.UserName == user.UserName && e.Password == hash.ToString());



    //    }

    //}

    public class LoginController : ControllerBase
    {
        public IConfiguration _configuration;
        private ITokenRefresh tokenRefreshBO;
        private readonly SIMMSContext _context;
        private ResponeResult responeResult = new ResponeResult();

        public LoginController(IConfiguration config, SIMMSContext context)
        {
            _configuration = config;
            _context = context;
        }

        #region API Methods login QUẢN TRỊ VIÊN

        [AllowAnonymous]
        [Route("api/login")]
        [HttpPost]
        public ResponeResult Login([FromBody] User loginModel)
        {

            try
            {
                if (loginModel == null)
                {
                    AddResponeError(ref responeResult, "", "Thông tin không hợp lệ! Vui lòng kiểm tra lại!");
                    return responeResult;
                }
                var dataresult = this.GetUser(loginModel.UserName, loginModel.Password);

                if (dataresult != null)
                {
                    // Prepare info for create Access Token
                    var tokeOptions = this.GenerateTokenOption(dataresult);

                    //// Prepare info for create Refresh Token
                    //TokenRefresh tokenRefresh = this.GenerateTokenRefresh(loginModel.UserName);

                    // Determite is create new or update refresh token
                    //bool isNewRefreshToken = string.IsNullOrEmpty(tokenRefresh.Id);

                    // Get user data to push into Session
                    Session _session = this.GetSession(dataresult.UserId);
                    HttpContext.Session.Set(Utils.Constant.SESSION_NAME, ToByteArray<Utils.Session>(_session));

                    // Create Access Token
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

                    // Save a token & refreshtoken into session object
                    HttpContext.Session.SetString(Utils.Constant.ACCESSTOKEN, tokenString);
                    //HttpContext.Session.SetString(Utils.Constant.REFRESHTOKEN, tokenRefresh.Refreshtoken);

                    // Return a brief user info for client
                    responeResult.RepData = new object[] { dataresult };
                    //this.tokenRefreshBO.Save(tokenRefresh, isNewRefreshToken);
                }
                else
                {
                    responeResult = new ResponeResult
                    { IsOk = false, MessageText = "Tên đăng nhập hoặc mật khẩu không chính xác!" };
                }

                return responeResult;
            }
            catch (Exception ex)
            {
                AddResponeError(ref responeResult, " Erro ", ex.ToString());
                return responeResult;
            }
        }



        private User GetUser(string username, string password)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(password));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return _context.User.FirstOrDefault(x => (x.UserName == username && x.Password == hash.ToString()));
        }


        private Utils.Session GetSession(int UserId)
        {
            try
            {
                Utils.Session session = new Utils.Session();
                var user = _context.User.FirstOrDefault(x => (x.UserId == UserId));
                if (user == null)
                    return null;

                //session.User = user;
                session.UserID = user.UserId;
                session.IsAdmin = true;
                session.IsLogin = true;
                return session;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private JwtSecurityToken GenerateTokenOption(User user)
        {
            // Basic claims
            Claim claimUserData = new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(user));
            Claim claimUserName = new Claim(ClaimTypes.Name, user.Email);
            List<Claim> listClaims = new List<Claim>();
            listClaims.Add(claimUserData);
            listClaims.Add(claimUserName);

            // Get token expired info from Constant
            string TokenExpireConfig = Constant.TOKENEXPIRED;
            if (string.IsNullOrEmpty(TokenExpireConfig))
                TokenExpireConfig = "86400";

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constant.JWTENCRYPTKEY));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
            var jwtSecurityToken = new JwtSecurityToken(
                issuer: Constant.ISUSER,
                audience: Constant.AUDIENCE,
                claims: listClaims,
                expires: DateTime.Now.AddSeconds(Convert.ToInt32(TokenExpireConfig)),
                signingCredentials: signinCredentials
            );


            return jwtSecurityToken;
        }

        private TokenRefresh GenerateTokenRefresh(string userName)
        {
            // Get theo TokenRefresh info from DB
            TokenRefresh tokenRefresh = this.tokenRefreshBO.GetData(userName);
            // Existed data
            if (tokenRefresh != null)
            {
                //renew refreshtoken
                tokenRefresh.Refreshtoken = Guid.NewGuid().ToString();
            }
            // not existed data
            else
            {
                // create new record
                tokenRefresh = new TokenRefresh
                {
                    Username = userName,
                    Refreshtoken = Guid.NewGuid().ToString()
                };
            }

            return tokenRefresh;
        }


        #endregion

        #region Logout
        [AllowAnonymous]
        [Route("api/logout")]
        [HttpPost]
        public ResponeResult Logout()
        {
            try
            {
                ResponeResult repData = new ResponeResult() { IsOk = true };
                HttpContext.Session.Clear();
                return repData;
            }
            catch (Exception ex)
            {
                return responeResult;
            }
        }
        #endregion



        #region Base setting

        [NonAction]
        public void AddResponeError(ref ResponeResult repData, string messageCode = "", string messageText = "", string messageError = "")
        {
            repData.IsOk = false;
            repData.MessageCode = messageCode;
            repData.MessageText = messageText;
            repData.MessageError = messageError;
        }

        [NonAction]
        public byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }


        #endregion


    }

}
