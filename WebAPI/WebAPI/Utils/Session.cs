﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Utils
{
    [Serializable]
    public class Session
    {
        public int UserID { get; set; }
        public int FiscalID { get; set; }
        public bool IsLogin { get; set; } = false;
        public bool IsAdmin { get; set; } = false;
        public bool IsSecondPassword { get; set; } = false;
    }
}
