﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Utils
{
    public class Constant
    {
        //Session infomation
        public const string SESSION_NAME = "SESSION_NAME";
        public const string ACCESSTOKEN = "ACCESSTOKEN";
        public const string REFRESHTOKEN = "REFRESHTOKEN";
        public const string JWTENCRYPTKEY = "sdfsdfsjdbf78sdyfssdfsdfbuidfs98gdfsdbf";
        public const string ISUSER = "JEEJOBAuthenticationServer";
        public const string AUDIENCE = "JEEJOBServicePostmanClient";
        public const string SUBJECT = "JEEJOBServiceAccessToken";
        public const string TOKENEXPIRED = "86400";//seconds
    }
}
