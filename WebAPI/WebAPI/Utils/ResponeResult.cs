﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Utils
{
    public class ResponeResult
    {
        public bool IsOk { get; set; } = true;
        public string MessageCode { get; set; }
        public string MessageText { get; set; }
        public string MessageError { get; set; }
        public dynamic RepData { get; set; }
        public int TotalRow { get; set; }
    }
}
