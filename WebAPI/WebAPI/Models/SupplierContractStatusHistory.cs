﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class SupplierContractStatusHistory
    {
        public int Id { get; set; }
        public int? SupplierContractId { get; set; }
        public int? StatusId { get; set; }
        public int? NextStatusId { get; set; }
        public string Notes { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public bool? IsDelete { get; set; }
        public string SearchString { get; set; }
    }
}
