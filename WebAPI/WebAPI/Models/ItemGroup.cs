﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class ItemGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Notes { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public string SearchString { get; set; }
        public double? SortOrder { get; set; }
    }
}
