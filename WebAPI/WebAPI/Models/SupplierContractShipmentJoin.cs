﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class SupplierContractShipmentJoin
    {
        public int Id { get; set; }
        public string BlCode { get; set; }
        public string Note { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
    }
}
