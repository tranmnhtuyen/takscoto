﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Position
    {
        public int Id { get; set; }
        public string PositionName { get; set; }
        public string PositionShortName { get; set; }
        public string Icon { get; set; }
        public string MailGroup { get; set; }
        public string Description { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
    }
}
