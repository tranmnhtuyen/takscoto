﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class UserDepartment
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? DepartmentId { get; set; }
        public int? PositionId { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
    }
}
