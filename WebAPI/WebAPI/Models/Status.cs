﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Status
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public string SearchString { get; set; }
        public int? SortOrder { get; set; }
        public string Description { get; set; }
    }
}
