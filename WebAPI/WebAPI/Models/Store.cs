﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Store
    {
        public int Id { get; set; }
        public string StoreName { get; set; }
        public string Address { get; set; }
        public bool? IsActive { get; set; }
    }
}
