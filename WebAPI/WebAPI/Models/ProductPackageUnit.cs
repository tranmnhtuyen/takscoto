﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class ProductPackageUnit
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int PackageUnitId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public int? SortOrder { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public string SearchString { get; set; }
    }
}
